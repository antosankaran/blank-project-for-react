import { Component } from '@angular/core';

@Component({
  selector: 'app-coffee',
  templateUrl: './coffee.component.html',
  styleUrls: ['./coffee.component.css']
})
export class CoffeeComponent {
  best1:string="assets/image/113633.jpg";
  best2:string="assets/image/109445.jpg";
  best3:string="assets/image/109446.jpg";
  best4:string="assets/image/109484.jpg";
  best5:string="assets/image/103624.jpg";
  best6:string="assets/image/102824.jpg";
  best7:string="assets/image/102823.jpg";
  best8:string="assets/image/102825.jpg";
  best9:string="assets/image/101539.jpg";
  best10:string="assets/image/100744.jpg";
  best11:string="assets/image/109483.jpg"

}
