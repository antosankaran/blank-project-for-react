import { Component } from '@angular/core';

@Component({
  selector: 'app-food',
  templateUrl: './food.component.html',
  styleUrls: ['./food.component.css']
})
export class FoodComponent {
  food1:string="assets/image/104108.jpg";
  food2:string="assets/image/113945.jpg";
  food3:string="assets/image/112589.jpg";
  food4:string="assets/image/112590.jpg";
  food5:string="assets/image/112591.jpg";
  food6:string="assets/image/109270.jpg";
  food7:string="assets/image/113943.jpg";
  food8:string="assets/image/113946.jpg";
  food9:string="assets/image/113941.jpg";
  food10:string="assets/image/113942.jpg";
  food11:string="assets/image/113944.jpg";
  food12:string="assets/image/112598.jpg";

}
