import { Component } from '@angular/core';

@Component({
  selector: 'app-merchandise',
  templateUrl: './merchandise.component.html',
  styleUrls: ['./merchandise.component.css']
})
export class MerchandiseComponent {
  mugs1:string="assets/image/113650_1.jpg";
  mugs2:string="assets/image/113713.jpg";
  mugs3:string="assets/image/111855.jpg";
  mugs4:string="assets/image/109297.jpg";
  cups1:string="assets/image/113699_1.jpg";
  cups2:string="assets/image/107500.jpg";
  cups3:string="assets/image/111484.jpg";
  cups4:string="assets/image/113573.jpg";
  tum1:string="assets/image/111480.jpg";
  tum2:string="assets/image/109766.jpg";
  tum3:string="assets/image/113388.jpg";
  tum4:string="assets/image/112930.jpg";
  water1:string="assets/image/113653_1.jpg";
  water2:string="assets/image/113720_1.jpg";
  water3:string="assets/image/113579.jpg";
  water4:string="assets/image/113975.jpg";
  station1:string="assets/image/111670.jpg";
  station2:string="assets/image/112930.jpg";
  access1:string="assets/image/109941.jpg";
  access2:string="assets/image/112734.jpg";
  access3:string="assets/image/112733.jpg";
  access4:string="assets/image/114061.jpg";

}
