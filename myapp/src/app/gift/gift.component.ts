import { Component } from '@angular/core';

@Component({
  selector: 'app-gift',
  templateUrl: './gift.component.html',
  styleUrls: ['./gift.component.css']
})
export class GiftComponent {
  coffee:string="assets/image/Shining_Pastel_Bannr_Icon_3_21af9aed58 (1).png";
  star:string="assets/image/Starbucks_Corporation_Logo_2011.svg.webp";
  and:string="assets/image/appstoreAndroid.png";
  ios:string="assets/image/appstoreiOS.png";
  india:string="assets/image/17d2c94d_1bbe_47d5_874c_c32af5bb2c18_fc9ce3422f.png";
  two:string="assets/image/71d3780c_be6e_46b1_ab01_8a2bce244a7f_1_26502c2483.png";
  red:string="assets/image/7c6f7c64_3f89_4ba2_9af8_45fc6d94ad35_df43378185.jpg";
  green:string="assets/image/0bed73e6_0d8d_48c0_8936_f46d7355a8ad_1_252a72464c.png";
  congrats:string="assets/image/97ee3280_2f05_43ad_bd94_f5c184d4f502_1_2d1b049023.png";
  thank:string="assets/image/720b9684_c1ac_49cb_92fe_a7e0240c9602_1_5d6c0c82c2.png"

}
