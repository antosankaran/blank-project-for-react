import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'myapp';
  starbucks:string="assets/image/Starbucks_Corporation_Logo_2011.svg.webp";
  login:string="assets/image/images.png"
}
