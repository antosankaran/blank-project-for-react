import { Component } from '@angular/core';

@Component({
  selector: 'app-drinks',
  templateUrl: './drinks.component.html',
  styleUrls: ['./drinks.component.css']
})
export class DrinksComponent {
  drinks1:string="assets/image/100433.jpg";
  drinks2:string="assets/image/104018.jpg";
  drinks3:string="assets/image/104014.jpg";
  drinks4:string="assets/image/100419.jpg";
  drinks5:string="assets/image/100377.jpg";
  drinks6:string="assets/image/100405.jpg";
  drinks7:string="assets/image/100391.jpg";
  drinks8:string="assets/image/104068.jpg";
  drinks9:string="assets/image/107934.jpg";
  drinks10:string="assets/image/107329.jpg";
  drinks11:string="assets/image/100512.jpg";
  drinks12:string="assets/image/103973.jpg";
  drinks13:string="assets/image/107933.jpg";
  drinks14:string="assets/image/100441.jpg";
  drinks15:string="assets/image/100385.jpg";
  drinks16:string="assets/image/100399.jpg";
  drinks17:string="assets/image/100427.jpg";
  drinks18:string="assets/image/104072.jpg";
  drinks19:string="assets/image/100511.jpg";
  drinks20:string="assets/image/107932.jpg";
  drinks21:string="assets/image/108052.jpg";
  drinks22:string="assets/image/100477.jpg";

}
