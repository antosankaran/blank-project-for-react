import { Component } from '@angular/core';

@Component({
  selector: 'app-bestseller',
  templateUrl: './bestseller.component.html',
  styleUrls: ['./bestseller.component.css']
})
export class BestsellerComponent {
  coffee1:string="assets/image/100501.jpg";
  coffee2:string="assets/image/100433.jpg";
  coffee3:string="assets/image/105468.jpg";
  coffee4:string="assets/image/103515.jpg";
  coffee5:string="assets/image/108177.jpg";
  coffee6:string="assets/image/100419.jpg";
  coffee7:string="assets/image/104362.jpg";
  coffee8:string="assets/image/107704.jpg";
  coffee9:string="assets/image/109270.jpg";
  coffee10:string="assets/image/100075.jpg";
  coffee11:string="assets/image/103689.jpg";
  coffee12:string="assets/image/107707.jpg"

}
