import { Component } from '@angular/core';

@Component({
  selector: 'app-mainfooter',
  templateUrl: './mainfooter.component.html',
  styleUrls: ['./mainfooter.component.css']
})
export class MainfooterComponent {
  star:string="assets/image/Starbucks_Corporation_Logo_2011.svg.webp";
  and:string="assets/image/appstoreAndroid.png"
  ios:string="assets/image/appstoreiOS.png"
}
