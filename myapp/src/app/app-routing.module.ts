import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BestsellerComponent } from './bestseller/bestseller.component';
import { CoffeeComponent } from './coffee/coffee.component';

import { DrinksComponent } from './drinks/drinks.component';
import { FoodComponent } from './food/food.component';
import { GiftComponent } from './gift/gift.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { MerchandiseComponent } from './merchandise/merchandise.component';
import { OrderComponent } from './order/order.component';
import { PayComponent } from './pay/pay.component';
import { ReadytoeatComponent } from './readytoeat/readytoeat.component';
import { SigninComponent } from './signin/signin.component';
import { SignupComponent } from './signup/signup.component';
import { StoreComponent } from './store/store.component';

const routes: Routes = [{path:'' ,component:HomeComponent},
{path:'gift',component:GiftComponent},{path:'order',component:OrderComponent},
{path:'pay',component:PayComponent},{path:'store',component:StoreComponent},
{path:'login',component:LoginComponent},{path:'bestseller',component:BestsellerComponent},
{path:'drinks',component:DrinksComponent},
{path:'food',component:FoodComponent},{path:'merchandise',component:MerchandiseComponent},
{path:'readytoeat',component:ReadytoeatComponent},{path:'coffee',component:CoffeeComponent},{path:'signup',component:SignupComponent}
,{path:'signin',component:SigninComponent}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
