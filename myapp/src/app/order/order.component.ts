import { Component } from '@angular/core';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent {
  o1:string="assets/image/100075.jpg";
  o2:string="assets/image/107707.jpg";
  o3:string="assets/image/103813.jpg";
  o4:string="assets/image/100468.jpg";
  o5:string="assets/image/105065.jpg";
  o6:string="assets/image/102857.jpg";
  o7:string="assets/image/114054.jpg";
  o8:string="assets/image/113941.jpg";
  o9:string="assets/image/113581.jpg";
  o10:string="assets/image/114048.jpg";
  o11:string="assets/image/100744.jpg";
  o12:string="assets/image/101995_1.jpg";

}
