import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { GiftComponent } from './gift/gift.component';
import { OrderComponent } from './order/order.component';
import { PayComponent } from './pay/pay.component';
import { StoreComponent } from './store/store.component';
import { LoginComponent } from './login/login.component';
import { ChangeDirective } from './change.directive';
import { BestsellerComponent } from './bestseller/bestseller.component';
import { DrinksComponent } from './drinks/drinks.component';
import { FoodComponent } from './food/food.component';

import { MerchandiseComponent } from './merchandise/merchandise.component';
import { ReadytoeatComponent } from './readytoeat/readytoeat.component';
import { CoffeeComponent } from './coffee/coffee.component';
import { FormsModule } from '@angular/forms';
import { SignupComponent } from './signup/signup.component';
import { SigninComponent } from './signin/signin.component';
import { FooterComponent } from './footer/footer.component';
import { MainfooterComponent } from './mainfooter/mainfooter.component';
import { SidenavComponent } from './sidenav/sidenav.component';
import { SearchComponent } from './search/search.component';
  
  
  

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    GiftComponent,
    OrderComponent,
    PayComponent,
    StoreComponent,
    LoginComponent,
    ChangeDirective,
    BestsellerComponent,
    DrinksComponent,
    FoodComponent,
    
    MerchandiseComponent,
    ReadytoeatComponent,
    CoffeeComponent,
    SignupComponent,
    SigninComponent,
    FooterComponent,
    MainfooterComponent,
    SidenavComponent,
    SearchComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,  
  ],
  providers: [],
  bootstrap: [AppComponent,]
})
export class AppModule { }
