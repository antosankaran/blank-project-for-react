import { Component } from '@angular/core';
 

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
bestseller:string="assets/image/Bestseller.jpg";
coffee:string="assets/image/CoffeeAtHome.jpg";
drinks:string="assets/image/Drinks.jpg";
food:string="assets/image/Food.jpg";
merchandise:string="assets/image/Merchandise.jpg";
ready:string="assets/image/ReadyToEat.jpg";
bite:string="assets/image/112601.webp";
cake:string="assets/image/7_1_793a1d6e49.png";
bottle:string="assets/image/Frappuccino_banner_1_4_722ac7022a.png";
milk:string="assets/image/3_2_9509ea1720.png";
chai:string="assets/image/Image_35_ff2c5f9905.png";
burger:string="assets/image/freshly_baked_cropped_c365e34a66.png";
bonus:string="assets/image/Bonus_Stars_2d6733cfde.png";
one:string="assets/image/112595.webp";
back:string="assets/image/web_ab5a0057ab.jpg";
star:string="assets/image/Starbucks_Corporation_Logo_2011.svg.webp";
and:string="assets/image/appstoreAndroid.png"
ios:string="assets/image/appstoreiOS.png"
}
